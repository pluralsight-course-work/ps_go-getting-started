package main

import (
	"fmt"
)

func main() {
	// declaration option 1
	var i int
	i = 55
	fmt.Println(i)

	// declaration option 2
	var pie float32 = 3.14159
	fmt.Println(pie)

	// declaration option 3 (most common)
	firstName := "King Arthur"
	fmt.Println(firstName)

	boolean := true
	fmt.Println(boolean)

	c := complex(3, 4)
	fmt.Println(c)

	real, imaginary := real(c), imag(c)
	fmt.Println(real, imaginary)
}
