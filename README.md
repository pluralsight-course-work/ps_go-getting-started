# Getting Started

```bash
# initialize the module
    # e.g. go mod init gitlab.com/pluralsight-course-work/ps_go-getting-started
go mod init <module name, usually the path to the repo>

# run thing program using the module identifier in 'go.mod'
go run gitlab.com/pluralsight-course-work/ps_go-getting-started
```
